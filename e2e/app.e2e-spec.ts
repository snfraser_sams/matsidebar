import { MatsidebarPage } from './app.po';

describe('matsidebar App', () => {
  let page: MatsidebarPage;

  beforeEach(() => {
    page = new MatsidebarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
