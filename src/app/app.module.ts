import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatIconModule, MatListModule, MatRadioModule, MatSidenavModule,
  MatSnackBarModule,
  MatToolbarModule
} from '@angular/material';
import { TypeSelectorPipe } from './type-selector.pipe';
import { TestDialogComponent} from './test/test.component';
import {RadioDialogComponent} from './test/radio.component';
import {FilterDialogComponent} from './test/filter.component';

@NgModule({
  declarations: [
    AppComponent,
    TypeSelectorPipe,
    TestDialogComponent,
    RadioDialogComponent,
    FilterDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatRadioModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [TestDialogComponent, RadioDialogComponent, FilterDialogComponent]
})
export class AppModule { }
