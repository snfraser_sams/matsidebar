import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'typeSelector'
})
export class TypeSelectorPipe implements PipeTransform {

  /**
   * Extract/filter gliders of specified class,
   * @param value The glider to test.
   * @param {string} type of glider e.g. sg or slocum
   * @return {any} The filtered glider.
   */
  transform(gliders: any[], type: string): any {
    return (gliders.filter(g => g.type===type));
  }

}
