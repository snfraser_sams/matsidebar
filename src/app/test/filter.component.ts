import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-test',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterDialogComponent implements OnInit {

  selectedFilter: string;

  constructor(public dialogRef: MatDialogRef<FilterDialogComponent>,
              public snackbar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) { }



  ngOnInit() {
  }

  // dispose of this dialog now....
  goaway(items) {
    console.log(items);
    this.dialogRef.close(items);
  }

  stay() {
    this.snackbar.open('Youve decided to stay a while, cheers !', 'Go Away');
    this.dialogRef.close();
  }

}
