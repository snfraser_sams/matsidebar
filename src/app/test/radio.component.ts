import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-test',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.css']
})
export class RadioDialogComponent implements OnInit {

  selectedWaypoint: string;

  constructor(public dialogRef: MatDialogRef<RadioDialogComponent>,
              public snackbar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: any) { }



  ngOnInit() {
  }

  // dispose of this dialog now....
  goaway() {
    console.log('Selected waypoint label: ' + this.selectedWaypoint);
    this.dialogRef.close(this.selectedWaypoint);
  }

  stay() {
    this.snackbar.open('Youve decided to stay a while, cheers !', 'Go Away');
  }

}
