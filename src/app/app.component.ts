import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig, MatSnackBar} from '@angular/material';
import {TestDialogComponent} from './test/test.component';
import {RadioDialogComponent} from './test/radio.component';
import {FilterDialogComponent} from './test/filter.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  allgliders = [
    {name: 'Scapa', id: 'SG123', type: 'sg', depth: 500, vis: true, dep: 'osnap8'},
    {name: 'Bellatrix', id: 'SG345', type: 'sg', depth: 700, vis: false, dep: 'eel6'},
    {name: 'Talisker', id: 'SG576', type: 'sg', depth: 850, vis: false, dep: 'eel6'},
    {name: 'Lagavulin', id: 'SG676', type: 'sg', depth: 230, vis: false, dep: 'osnap8'},
    {name: 'Denebola', id: 'SG345', type: 'sg', depth: 1000, vis: true, dep: 'eel6'},
    {name: 'Bowmore', id: 'Unit_303', type: 'slocum', depth: 1500, vis: true, dep: 'osnap8'},
    {name: 'Ardbeg', id: 'Unit_504', type: 'slocum', depth: 2500, vis: true, dep: 'osnap8'},
    {name: 'BoatyMcBoat', id: 'Unit_367', type: 'slocum', depth: 1500, vis: true, dep: 'eel6'},
    {name: 'PinkGlider', id: 'Unit_234', type: 'slocum', depth: 2000, vis: false, dep: 'eel6'}
  ];

  gliders = [];

  posns = [
    {name: 'Wpt1',    lat: 55.45, lon: -6.4},
    {name: 'Uist-a',  lat: 56.56, lon: -6.8},
    {name: 'Barra-b', lat: 57.33, lon: -6.2},
    {name: 'Rockall', lat: 57.93, lon: -6.5},
    {name: 'Dorn-s',  lat: 58.42, lon: -6.9},
    {name: 'Mingu-2', lat: 56.77, lon: -7.3},
    {name: 'Hirta',   lat: 56.44, lon: -7.7},
    {name: 'Bor-3',   lat: 57.22, lon: -8.2},
    {name: 'Nort-a',  lat: 56.23, lon: -7.9},
    {name: 'Troug-1', lat: 57.88, lon: -8.1}
  ];

  deployments = [
    {name: 'MASSMO-2'},
    {name: 'OSNAP-8'},
    {name: 'EEL-6'},
    {name: 'BoBBlE-1'}
  ];

  missions = [
    {name: 'Testing1'},
    {name: 'Rockall-Rendezvous'},
    {name: 'NorthSeaTrip'},
    {name: 'CalibRun'}
  ];

  versions = [
    {name: '1'},
    {name: '2'}
  ];

  appName = '';

  gliderName = '';

  constructor(public dialog: MatDialog, public snackbar: MatSnackBar) {}

  ngOnInit() {
    this.gliders = this.allgliders;
  }

  selectApp(event) {
    console.log('Select: ' + event);
    switch (event) {
      case 1:
        this.appName = 'Glider Terminal';
        break;
      case 2:
        this.appName = 'Mission Planner';
        break;
      case 3:
        this.appName = 'Data Display';
        break;
    }
  }

  selectGlider(g: any) {
    this.gliderName = g.name + ' / ' + g.id + ' Max depth: ' + g.depth + 'm';
  }

  popup1() {
    // popup a modal dialog with some data
    const dialogref = this.dialog.open(TestDialogComponent, {data: {positions: this.posns}});


  }

  popup2() {
    // popup a modal dialog with some data
    const dialogref = this.dialog.open(RadioDialogComponent, {data: {positions: this.posns}});
    dialogref.afterClosed().subscribe(
      data => this.snackbar.open('You selected the waypoint: ' + data, 'Close', {duration: 1000})
    );
  }

  popup3() {
    const dialogref = this.dialog.open(FilterDialogComponent);
    dialogref.afterClosed().subscribe(
      data => this.snackbar.open('You selected the filter: ' + data, 'Close', {duration: 5000})
    );
  }

  applyFilter(x) {
    console.log('Apply filter mode: ' + x);
    switch (x) {
      case 1:
        this.gliders = this.allgliders;
        break;
      case 2:
        this.gliders = this.allgliders.filter((g) => g.dep === 'osnap8');
        break;
      case 3:
        this.gliders = this.allgliders.filter((g) => g.depth > 1000);
        break;
      case 4:
        this.gliders = this.allgliders.filter((g) => g.vis === true);
        break;
      case 5:
        this.gliders = this.allgliders.filter((g) => g.type === 'slocum');
        break;
      default:

    }

  }

}
